This package supports mobx state management with built-in functions.
> Heyy, before starting to create store. Here I have some small rules, in the store if you name the variable starting with `_` this time the model will treat it as a temporary value, you will not get it in the `submit()` function unless You intentionally want to use it : )

You can see [demo](https://codesandbox.io/s/elastic-cloud-k7qqu) here!
---

# How to use

1. Use with store

```js
    import { createViewModel, IViewModel } from 'mobx-model-xd';
    class MobxClass {
        @observable
        ...
    }

    const instanceofClass = new MobxClass();
    const model = createViewModel(instanceofClass);
    export default model;
    // You can use type here
    export type TMobxClass = typeof model;
```

2. Use with mini store

```js
    type TStore = {
        name: string
    }

    const store = createViewModel<TStore>(observable({
        name: "Beau.vn"
    }));

```
And now, you have super state management functions.

Here, I will take the example with model with store case.
## Help functions

1. Update store

```js
    // Update single key
    TMobxClass.updateStore({ key: 'key1', value: value1 });
    // Update multiple keys
    TMobxClass.updateStore({ key: ['key1', 'key2', 'key3'], value: [value1, value2, value3] });
```

2. Update depth store

```js
    TMobxClass.updateDeepStore('path.to.store', value);
```

3. Get properties of store

```js
    // Get all variables except those starting with `_`
    TMobxClass.submit();
    // Or you can get specific variables or a series of variables in the state
    TMobxClass.submit('variable1');
    // List variable in store
    TMobxClass.submit(['variable1', 'variable2']);
```

4. Similar to `reset()`, except that the reset will keep things original
> However, you can also exclude attributes you do not want to reset

```js
    TMobxClass.reset('variable1', `variables you don't want to reset`);
    // or
    TMobxClass.reset('variable1', ['variables1', 'variables2']);
```
> If the first parameter is `undefined` mobx-xD will reset all variables in the store
5. Or you can use it to upload files

```js
    // Example
    interface IImage {
    destination: string;
    encoding: string;
    fieldname: string;
    filename: string;
    mimetype: string;
    originalname: string;
    path: string;
    size: number;
    }
    await TMobxClass.uploadFile<IImage[]>(files: File[], url?: string, field?: string);
```

6. Get model
You can use this method for get model data

```js
    TMobxClass.getModel();
```

### Mobx also supports other methods
```js
    import { get, post, put, del } from 'mobx-model-xd';
```

Now you can use update, reset, uploadFile from model
#### Thanks
- Thanks ITNEXT for share this [post](https://itnext.io/step-by-step-building-and-publishing-an-npm-typescript-package-44fe7164964c)

---
Author: [DungNguyen](https://www.facebook.com/nihilismJS/)
