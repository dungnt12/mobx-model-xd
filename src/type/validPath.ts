export type PathOfLength1<T, K1 extends keyof T> = [K1];
export type PathOfLength2<T, K1 extends keyof T, K2 extends keyof T[K1]> = [
  K1,
  K2,
];
export type PathOfLength3<
  T,
  K1 extends keyof T,
  K2 extends keyof T[K1],
  K3 extends keyof T[K1][K2]
> = [K1, K2, K3];
export type PathOfLength4<
  T,
  K1 extends keyof T,
  K2 extends keyof T[K1],
  K3 extends keyof T[K1][K2],
  K4 extends keyof T[K1][K2][K3]
> = [K1, K2, K3, K4];
export type PathOfLength5<
  T,
  K1 extends keyof T,
  K2 extends keyof T[K1],
  K3 extends keyof T[K1][K2],
  K4 extends keyof T[K1][K2][K3],
  K5 extends keyof T[K1][K2][K3][K4]
> = [K1, K2, K3, K4, K5];
export type PathOfLength6<
  T,
  K1 extends keyof T,
  K2 extends keyof T[K1],
  K3 extends keyof T[K1][K2],
  K4 extends keyof T[K1][K2][K3],
  K5 extends keyof T[K1][K2][K3][K4],
  K6 extends keyof T[K1][K2][K3][K4][K5]
> = [K1, K2, K3, K4, K5, K6];
export type PathOfLength7Plus<
  T,
  K1 extends keyof T,
  K2 extends keyof T[K1],
  K3 extends keyof T[K1][K2],
  K4 extends keyof T[K1][K2][K3],
  K5 extends keyof T[K1][K2][K3][K4],
  K6 extends keyof T[K1][K2][K3][K4][K5],
  K7 extends keyof T[K1][K2][K3][K4][K5][K6]
> = [K1, K2, K3, K4, K5, K6, K7, ...(number | string)[]];

/**
 * A function to validate that a path is valid for a variable of type T.
 * It simply returns the path itself, and doesn't do anything. The main value
 * is that it infers the types via function overloading.
 *
 * The best way to use this is to not explicitly state any types in the
 * generics and to pass a dummy value of the type the path should apply to
 * and a path.
 *
 * If the path is invalid, it won't typecheck. Unfortunately, the autocomplete
 * for `path` doens't work super well though.
 */
export function validatePath<T, K1 extends keyof T>(
  _dummyValue: T,
  path: PathOfLength1<T, K1>,
): PathOfLength1<T, K1>;

export function validatePath<T, K1 extends keyof T, K2 extends keyof T[K1]>(
  _dummyValue: T,
  path: PathOfLength2<T, K1, K2>,
): PathOfLength2<T, K1, K2>;

export function validatePath<
  T,
  K1 extends keyof T,
  K2 extends keyof T[K1],
  K3 extends keyof T[K1][K2]
>(
  _dummyValue: T,
  path: PathOfLength3<T, K1, K2, K3>,
): PathOfLength3<T, K1, K2, K3>;

export function validatePath<
  T,
  K1 extends keyof T,
  K2 extends keyof T[K1],
  K3 extends keyof T[K1][K2],
  K4 extends keyof T[K1][K2][K3]
>(
  _dummyValue: T,
  path: PathOfLength4<T, K1, K2, K3, K4>,
): PathOfLength4<T, K1, K2, K3, K4>;

export function validatePath<
  T,
  K1 extends keyof T,
  K2 extends keyof T[K1],
  K3 extends keyof T[K1][K2],
  K4 extends keyof T[K1][K2][K3],
  K5 extends keyof T[K1][K2][K3][K4]
>(
  _dummyValue: T,
  path: PathOfLength5<T, K1, K2, K3, K4, K5>,
): PathOfLength5<T, K1, K2, K3, K4, K5>;

export function validatePath<
  T,
  K1 extends keyof T,
  K2 extends keyof T[K1],
  K3 extends keyof T[K1][K2],
  K4 extends keyof T[K1][K2][K3],
  K5 extends keyof T[K1][K2][K3][K4],
  K6 extends keyof T[K1][K2][K3][K4][K5]
>(
  _dummyValue: T,
  path: PathOfLength6<T, K1, K2, K3, K4, K5, K6>,
): PathOfLength6<T, K1, K2, K3, K4, K5, K6>;

export function validatePath<
  T,
  K1 extends keyof T,
  K2 extends keyof T[K1],
  K3 extends keyof T[K1][K2],
  K4 extends keyof T[K1][K2][K3],
  K5 extends keyof T[K1][K2][K3][K4],
  K6 extends keyof T[K1][K2][K3][K4][K5],
  K7 extends keyof T[K1][K2][K3][K4][K5][K6]
>(
  _dummyValue: T,
  path: PathOfLength7Plus<T, K1, K2, K3, K4, K5, K6, K7>,
): PathOfLength7Plus<T, K1, K2, K3, K4, K5, K6, K7>;

export function validatePath<T>(_dummyValue: T, path: unknown): unknown {
  return path;
}