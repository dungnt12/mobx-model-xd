// -----------------------------------------
// ----- FETCH FUNCTION FOR  --------
// -----------------------------------------

// Interface
type THeader = { [key: string]: string };

export interface HttpResponse<T> extends Response {
    data?: T;
  }
  /**
   * ### Example
   * ```
      let response: HttpResponse<Todo[]>;
      try {
        response = await http<Todo[]>(
          "https://jsonplaceholder.typicode.com/todosX"
        );
        console.log("response", response);
      } catch (response) {
        console.log("Error", response);
      }
   * ```
   */
  export async function http<T>(
    request: RequestInfo,
    body?: any,
    headers: THeader = {
      'Content-Type': 'application/json',
    },
  ): Promise<HttpResponse<T>> {
    const response: HttpResponse<T> = await fetch(
      request,
      { headers, body },
    );
  
    try {
      response.data = await response.json();
    } catch (ex) {
        throw ex;
    }
  
    if (!response.ok) {
      throw new Error(response.statusText);
    }
    return response;
  }
  
  /**
   *  ### Example
      ```
      const { data } = await get<Todo[]>(
        "https://jsonplaceholder.typicode.com/posts",
        'accessToken'
      );
      ```
   */
  export async function get<T>(
    path: string,
    args: RequestInit = { method: 'get' },
  ): Promise<HttpResponse<T>> {
    const result = await http<T>(new Request(path, args));
    return result;
  }
  
  /**
   *  ### Example
      ```
      const { data } = await post<{ id: number }>(
        "https://jsonplaceholder.typicode.com/posts",
        { title: "my post", body: "some content" }
      );
      ```
   */
  export async function post<T>(
    path: string,
    body: any,
    args: RequestInit = { method: 'get' },
  ): Promise<HttpResponse<T>> {
    const result = await http<T>(new Request(path, args), body);
    return result;
  }
  
  /**
   *  ### Example
      ```
      const { data } = await put<{ id: number }>(
        "https://jsonplaceholder.typicode.com/posts",
        { title: "my post", body: "some content" }
      );
      ```
   */
  export async function put<T>(
    path: string,
    body: any,
    args: RequestInit = { method: 'put', body },
  ): Promise<HttpResponse<T>> {
    const result = await http<T>(new Request(path, args), body);
    return result;
  }
  
  /**
   *  ### Example
      ```
      const { data } = await del<Todo[]>(
        "https://jsonplaceholder.typicode.com/posts",
      );
      ```
   */
  export async function del<T>(
    path: string,
    args: RequestInit = { method: 'delete' },
  ): Promise<HttpResponse<T>> {
    const result = await http<T>(new Request(path, args));
    return result;
  }
  