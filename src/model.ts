/*
    Model con được gọi dưới dạng `this.model`, mỗi hàm trong helper này đều cần update cho `this.model` thay vì `this`
    Khi đặt tên store bao gồm cả những biến dùng tạm (sẽ ko lấy khi gọi `submit()`) những biến này sẽ bắt đầu bằng `_`
*/

import {
  action,
  ObservableMap,
  IComputedValue,
  observable,
  isObservableObject,
  isComputedProp,
  computed,
  _getAdministration,
  $mobx,
  toJS,
} from 'mobx';

import { invariant, getAllMethodsAndProperties } from './utils';
import { URL_UPLOAD_FILE, FIELD_UPLOAD } from './config';
import { post } from './core/helper/fetch';

export interface IViewModel<T> {
  model: T;
  /**
      Reset các thuộc tính trong store
     */
  reset(byKey?: keyof T | (keyof T)[], ignore?: keyof T | (keyof T)[]): void;
  /**
      Lấy biến trong store, nếu không truyền gì thì lấy tất cả biến trừ biến bắt đầu bằng `_`
     */
  submit(byKey?: keyof T | (keyof T)[]): T;
  /**
      Upload file lên server của mình, config tại `./config.ts`
      Thường dùng với dropzone
    */
  uploadFile<T>(file: File[], url?: string, field?: string): Promise<T | Error>;
  /**
      #### Update store tương ứng với từng key
      ```js
      updateStore('key', value) || updateStore('key1 key2 key3', [value1, value2, value3])
      ```
     */
  updateStore: ({ key, value }: { key: (keyof T)[]|keyof T; value: any }) => void;
  /**
      Update object theo path, thường dùng để update object lồng nhau
      ##### Example
      ```js
        updateDeepState('code.ts.awesome', true);
      ```
    */
  updateDeepStore: (path: string, value: any, setrecursively?: boolean) => void;
  /**
   * Toggle property in store
   */
  toggleMe: (key: keyof T, value?: boolean) => void;
  /**
   * Lấy toàn bộ store trong model
   */
  getModel: () => T;
}


export class ViewModel<T> implements IViewModel<T> {
  private modelLocal: T;

  localValues: ObservableMap<keyof T, T[keyof T]> = observable.map({});

  localComputedValues: ObservableMap<keyof T, IComputedValue<T[keyof T]>> = observable.map({});

  constructor(public model: T) {

    invariant(isObservableObject(model), 'createViewModel expects an observable object');

    // use this helper as Object.getOwnPropertyNames doesn't return getters
    getAllMethodsAndProperties(model).forEach((key: any) => {
      if (key === ($mobx as any) || key === '__mobxDidRunLazyInitializers') {
        return;
      }

      if (isComputedProp(model, key)) {
        const { derivation } = _getAdministration(model, key); // Fixme: there is no clear api to get the derivation
        this.localComputedValues.set(key, computed(derivation.bind(this)));
      }

      const descriptor = Object.getOwnPropertyDescriptor(model, key);
      const additionalDescriptor = descriptor ? { enumerable: descriptor.enumerable } : {};

      Object.defineProperty(this, key, {
        ...additionalDescriptor,
        configurable: true,
        get: () => {
          if (isComputedProp(model, key)) return this.localComputedValues.get(key)!.get();
          return this.model[key as keyof T];
        },
        set: action((value: any) => {
          if (value !== this.model[key as keyof T]) {
            this.localValues.set(key, value);
          } else {
            this.localValues.delete(key);
          }
        }),
      });
    });

    this.modelLocal = JSON.parse(JSON.stringify(this.model));
  }

  @action.bound
  toggleMe(key: keyof T, value?: boolean) {
    const self: { [key: string]: any } = this.model;
    if (value !== undefined) {
      self[key as string] = value;
    } else {
      self[key as string] = !self[key as string];
    }
  }

  @action.bound
  submit(byKey?: keyof T | (keyof T)[]) {
    const self = toJS(this.model);
    const modelResult: any = {};
    Object.keys(self).forEach((key) => {
      if (byKey) {
        switch (typeof byKey) {
          case 'string': {
            if (key === byKey) modelResult[key as keyof T] = self[key as keyof T];
            break;
          }
          case 'object': {
            if (byKey.includes(key as keyof T)) modelResult[key as keyof T] = self[key as keyof T];
            break;
          }
          default:
            break;
        }
      } else if (key.charAt(0) !== '_') {
        modelResult[key as keyof T] = self[key as keyof T];
      }
    });
    return modelResult;
  }

  @action.bound
  reset(byKey?: keyof T | (keyof T)[], ignore?: keyof T | (keyof T)[]) {
    const { modelLocal } = this;
    const self = this.model as { [index: string]: any };
    Object.keys(modelLocal).forEach((key) => {
      if (byKey) {
        switch (typeof byKey) {
          case 'string': {
            if (key === byKey) self[key] = modelLocal[key as keyof T];
            break;
          }
          case 'object': {
            if (byKey.includes(key as keyof T)) self[key] = modelLocal[key as keyof T];
            break;
          }
          default:
            break;
        }
      } else if (!ignore) {
        self[key] = modelLocal[key as keyof T];
      }
      if (ignore) {
        switch (typeof ignore) {
          case 'string': {
            if (key !== ignore) {
              self[key] = modelLocal[key as keyof T];
            }
            break;
          }
          case 'object': {
            if (!ignore.includes(key as keyof T)) {
              self[key] = modelLocal[key as keyof T];
            }
            break;
          }
          default:
            break;
        }
      }
    });
  }

  @action.bound
  updateStore = ({ key, value }: { key: (keyof T)[]|keyof T; value: any }): void => {
    if (Array.isArray(key)) {
      key.forEach((key1, index) => {
        this.model[key1] = value[index];
      });
    }
    if (typeof key === 'string') {
      this.model[key] = value;
    }
  };

  @action.bound
  updateDeepStore = (pathOrigin: string, value: any, setrecursively = false) => {
    const self = this.model as Object;
    const path: string[] = pathOrigin.split('.');
    let level = 0;

    path.reduce((a: any, b: any) => {
      level += 1;

      if (setrecursively && typeof a[b] === 'undefined' && level !== path.length) {
        a[b] = {};
        return a[b];
      }

      if (level === path.length) {
        a[b] = value;
        return value;
      }
      return a[b];
    }, self);
  };

  @action
  // eslint-disable-next-line class-methods-use-this
  async uploadFile<T>(files: File[], url?: string, field?: string): Promise<T | Error> {
    const formData = new FormData();
    files.forEach((file: File) => {
      formData.append(field || FIELD_UPLOAD, file);
    });

    try {
      const { data } = await post<T>(url || URL_UPLOAD_FILE, formData, undefined);
      if (data) return data;
    } catch (error) {
      throw new Error(error);
    }
    return new Error('Something error :(');
  }
  @action.bound
  getModel() {
    return this.model;
  }
}

export function createViewModel<T>(model: T): T & IViewModel<T> {
  const result = new ViewModel(model) as any;
  return result as T & IViewModel<T>;
}

export * from './core/helper/fetch';