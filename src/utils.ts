export function invariant(cond: boolean, message = 'Illegal state') {
  if (!cond) console.error(message);
}
const deepFields = (x: any): any => {
  return (
    x && x !== Object.prototype && Object.getOwnPropertyNames(x).concat(deepFields(Object.getPrototypeOf(x)) || [])
  );
};
const distinctDeepFields = (x: any) => {
  const deepFieldsIndistinct = deepFields(x);
  const deepFieldsDistinct = deepFieldsIndistinct.filter(
    (item: any, index: number) => deepFieldsIndistinct.indexOf(item) === index,
  );
  return deepFieldsDistinct;
};

// eslint-disable-next-line no-bitwise
export const getAllMethodsAndProperties = (x: any): any =>
  distinctDeepFields(x).filter((name: string) => name !== 'constructor' && !~name.indexOf('__'));
